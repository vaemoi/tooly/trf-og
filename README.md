# trf-og

A rewrite of the GNU Linux command [`tr`](http://bxr.su/OpenBSD/usr.bin/tr/) for npm projects. Active development here [here](https://gitlab.com/vaemoi/tooly/trf-og)

Deprecated. Use `npm install @vaemoi/trf` instead
